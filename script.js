$("#content-1").show();
$("#content-2").hide();
$("#content-3").hide();
$("#content-4").hide();
$("#tab-1").click(function(){
    $("#tab-1").addClass("selected");
    $("#tab-2").removeClass("selected");
    $("#tab-3").removeClass("selected");
    $("#tab-4").removeClass("selected");
    $("#content-1").show();
    $("#content-2").hide();
    $("#content-3").hide();
    $("#content-4").hide();
});

$("#tab-2").click(function(){
    $("#tab-1").removeClass("selected");
    $("#tab-2").addClass("selected");
    $("#tab-3").removeClass("selected");
    $("#tab-4").removeClass("selected");
    $("#content-1").hide();
    $("#content-2").show();
    $("#content-3").hide();
    $("#content-4").hide();
});

$("#tab-3").click(function(){
    $("#tab-1").removeClass("selected");
    $("#tab-2").removeClass("selected");
    $("#tab-3").addClass("selected");
    $("#tab-4").removeClass("selected");
    $("#content-1").hide();
    $("#content-2").hide();
    $("#content-3").show();
    $("#content-4").hide();
});

$("#tab-4").click(function(){
    $("#tab-1").removeClass("selected");
    $("#tab-2").removeClass("selected");
    $("#tab-3").removeClass("selected");
    $("#tab-4").addClass("selected");
    $("#content-1").hide();
    $("#content-2").hide();
    $("#content-3").hide();
    $("#content-4").show();
});

$(".work-content").scroll(function (){
    if ($(".work-content").scrollTop()  <= 0 ){
        $(".scroll-icon").css("visibility","visible !important");
    }
    $(".scroll-icon").css("visibility","hidden");
});

var slideIndex = 1;
showSlides(slideIndex);


function move(n) {
    showSlides(slideIndex += n);
}


function paginationSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = $(".gallery-picture");
    var pagination = $(".pagination");

    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
        slides[i].className = slides[i].className.replace(" selected", "");
    }
    for (i = 0; i < pagination.length; i++) {
        pagination[i].className = pagination[i].className.replace(" active", "");
    }

    if (n > slides.length || n == 1) {
        slideIndex = 1
        slides[0].className += " selected";
        slides[0].style.display = "inline-block";
        slides[1].style.display = "inline-block";
        slides[2].style.display = "inline-block";
        pagination[0].className += " active";
    } else if (n < 1 || n == slides.length) {
        slideIndex = slides.length
        slides[slideIndex-1].className += " selected";
        slides[slideIndex-3].style.display = "inline-block";
        slides[slideIndex-2].style.display = "inline-block";
        slides[slideIndex-1].style.display = "inline-block";
        pagination[slideIndex-1].className += " active";
    } else {
        slides[slideIndex-1].className += " selected";
        slides[slideIndex-2].style.display = "inline-block";
        slides[slideIndex-1].style.display = "inline-block";
        slides[slideIndex].style.display = "inline-block";
        pagination[slideIndex-1].className += " active";
    }
}